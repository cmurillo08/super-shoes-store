<?php

namespace App\Http\Controllers;

use App\Article;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        return View::make('articles.index')
            ->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = $this->getStoresToDisplay();
        return View::make('articles.create')
            ->with('stores', $stores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields($request);

        $article = new Article;
        $article->name = Input::get('name');
        $article->description = Input::get('description');
        $article->price = Input::get('price');
        $article->total_in_shelf = Input::get('total_in_shelf');
        $article->total_in_vault = Input::get('total_in_vault');
        $article->store_id = Input::get('store');
        $article->save();

        // redirect
        Session::flash('message', 'Successfully created article!');
        return Redirect::to('articles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $stores = $this->getStoresToDisplay();
        // show the edit form and pass the article and stores
        return View::make('articles.edit')
            ->with('article', $article)
            ->with('stores', $stores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validateFields($request);
        
        $article->name = Input::get('name');
        $article->description = Input::get('description');
        $article->price = Input::get('price');
        $article->total_in_shelf = Input::get('total_in_shelf');
        $article->total_in_vault = Input::get('total_in_vault');
        $article->store_id = Input::get('store');
        $article->save();

        // redirect
        Session::flash('message', 'Successfully updated the article!');
        return Redirect::to('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the article!');
        return Redirect::to('articles');
    }

    private function getStoresToDisplay() {
        $stores = Store::all();
        $stores_info = [];

        foreach ($stores as $key => $store) {
            $stores_info[$store->id] = $store->name;
        }

        return $stores_info;
    }

    private function validateFields(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'total_in_shelf' => 'required',
            'total_in_vault' => 'required',
            'store' => 'required'
        ]);
    }
}
