<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();

        // load the view and pass the stores
        return View::make('stores.index')
            ->with('stores', $stores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields($request);

        $store = new Store;
        $store->name = Input::get('name');
        $store->address = Input::get('address');
        $store->save();

        // redirect
        Session::flash('message', 'Successfully created store!');
        return Redirect::to('stores');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        // show the edit form and pass the store
        return View::make('stores.edit')
            ->with('store', $store);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $this->validateFields($request);

        $store->name = Input::get('name');
        $store->address = Input::get('address');
        $store->save();
        // redirect
        Session::flash('message', 'Successfully updated the store!');
        return Redirect::to('stores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the store!');
        return Redirect::to('stores');
    }

    private function validateFields(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required'
        ]);
    }
}
