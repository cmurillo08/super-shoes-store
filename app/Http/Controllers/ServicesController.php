<?php

namespace App\Http\Controllers;

use App\Store;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServicesController extends Controller
{
	 /**
     * Return a list of stores.
     *
     * @return \Illuminate\Http\Response
     */
    public function stores()
    {
        $stores = Store::all();

		return Response()->json(array(
            'success' => true,
            'stores' => $stores,
            'total_elements' => $stores->count()
        ));
    }

     /**
     * Return a list of articles.
     *
     * @return \Illuminate\Http\Response
     */
    public function articles()
    {
        $articles = Article::all();

		return Response()->json(array(
            'success' => true,
            'articles' => $articles,
            'total_elements' => $articles->count()
        ));
	}

	 /**
     * Return a list of articles for a specified store.
     *
     * @return \Illuminate\Http\Response
     */
    public function store_articles($id)
    {
        $store = Store::find($id);
        if(!is_null($store) && is_numeric($id)) {
        	$articles = $store->articles;
        	return Response()->json(array(
		        'success' => true,
		        'articles' => $articles,
		        'total_elements' => $articles->count()
	    	));
        } else {
        	$error_code = is_numeric($id) ? 404 : 400;
        	$error_msg = is_numeric($id) ? 'Record not Found' : 'Bad Request';
        	return Response()->json(array(
		        'error_code' => $error_code,
		        'error_msg' => $error_msg,
		        'success' => false,
	    	));
        }
	}
}