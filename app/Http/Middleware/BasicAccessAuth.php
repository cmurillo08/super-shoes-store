<?php

namespace App\Http\Middleware;

use Closure;

class BasicAccessAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->basic_validate($request->header('API_AUTH_USER'), $request->header('API_AUTH_PW'))) {
            return Response()->json(array(
                'error_code' => 401,
                'error_msg' => 'Not authorized',
                'success' => false,
            ));
        }
        return $next($request);
    }

    private function basic_validate($user, $password)
    {
        if ($user === 'my_user' && $password === 'my_password') {
            return true;
        }

        return false;
    }
}
