<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stores';
	protected $hidden = ['created_at','updated_at'];
    /**
     * Get the articles for the store.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }    
}
