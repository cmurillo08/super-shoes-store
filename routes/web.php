<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StoreController@index');
Route::resource('stores', 'StoreController');
Route::resource('articles', 'ArticleController');
Route::get('services/stores', 'ServicesController@stores')->middleware('basicaccess');
Route::get('services/stores/{id}/articles', 'ServicesController@store_articles')->middleware('basicaccess');
Route::get('services/articles', 'ServicesController@articles')->middleware('basicaccess');