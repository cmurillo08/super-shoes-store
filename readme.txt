1. Install composer
2. Install laravel
3. Set your database configuration in config/database
4. Enable mod_rewrite in apache configuration this is require for the .htacces file that remove the index.php
from the url.
5. laravelcollective ^5.4.0 set on composer.json to support blade Form and Html: "lravelcollective/html": "^5.4.0"
6. Provider added on config/app -> Collective\Html\HtmlServiceProvider::class
Aliases added:
'Form' => Collective\Html\FormFacade::class,
'Html' => Collective\Html\HtmlFacade::class
7. Run (on mac .vendor/bin/phpunit) or (on windows .vendor\bin\phpunit) to test the services (test/Featutre/ServicesTest.php)
8. run php artisan serve to start the app
9. Access http://localhost:8000 to mantain the database information
10. Access the services using Postman or other API testing tool:
http://localhost:8000/services/stores
http://localhost:8000/services/articles
http://localhost:8000/services/stores/{id}/articles
You will need to pass those http headers for basic access authentication
API_AUTH_USER=my_user
API_AUTH_PW=my_password
