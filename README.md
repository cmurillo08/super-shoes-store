# README #

1. Composer should be installed.
2. Laravel should be installed.
3. Enable mod_rewrite in apache configuration this is require for the .htacces file that remove the index.php
from the url.
4. Download or checkout the Super Shoes Store.
5. Go to the root project folder.
6. Run composer install.
7. Run composer update.
8. Copy .env.example to .env (linux cp .env.example .env) or (on windows copy .env.example .env)
9. Set your database configuration in .env
10. Run php artisan migrate to run all the database migrations.
11. Run (on linux ./vendor/bin/phpunit) or (on windows .\vendor\bin\phpunit) to test the services (test/Featutre/ServicesTest.php)
12. Run php artisan serve to start the app.
13. Access http://localhost:8000 to mantain the database information.
14. Access the services using Postman or other API testing tool:
http://localhost:8000/services/stores
http://localhost:8000/services/articles
http://localhost:8000/services/stores/{id}/articles
You will need to pass those http headers for basic access authentication
API_AUTH_USER=my_user and API_AUTH_PW=my_password