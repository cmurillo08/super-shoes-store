<?php

namespace Tests\Feature;

use App\Store;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServicesTest extends TestCase
{
	
    public function testServicesAreListedCorrectly()
    {
        $headers = ['API_AUTH_USER' => 'my_user', 'API_AUTH_PW' => 'my_password'];
        //Getting stores
        $response = $this->json('GET', '/services/stores', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'stores' => ['*' => ['id', 'name', 'address']],
                'success',
                'total_elements'
                ]
            );
        //Getting articles
        $response = $this->json('GET', '/services/articles', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'articles' => ['*' => ['id', 'name', 'description', 'price', 'total_in_shelf', 'total_in_vault', 'store_id']],
                'success',
                'total_elements'
                ]
            );            
    }

    public function testServicesErrors()
    {
    	//User is not authorized
        $response = $this->json('GET', '/services/stores')
            ->assertStatus(200)
             ->assertJson([
                'error_code' => '401',
                'error_msg' => 'Not authorized',
                'success' => false,
            ]);
        //Wrong parameters (id is not a number)
        $headers = ['API_AUTH_USER' => 'my_user', 'API_AUTH_PW' => 'my_password'];
        $response = $this->json('GET', '/services/stores/abc/articles', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'error_code' => '400',
                'error_msg' => 'Bad Request',
                'success' => false,
            ]);
        //No store with that ID
        $headers = ['API_AUTH_USER' => 'my_user', 'API_AUTH_PW' => 'my_password'];
        $response = $this->json('GET', '/services/stores/99/articles', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'error_code' => '404',
                'error_msg' => 'Record not Found',
                'success' => false,
            ]);
    }
}
