<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Shoes</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
		html {
			position: relative;
			min-height: 100%;
		}
		body {
			/* Margin bottom by footer height */
			margin-bottom: 60px;
		}
		h1 {
			color: #337ab7;
			text-align: center;
		}
		.footer {
			position: absolute;
			padding: 20px;
			bottom: 0;
			width: 100%;
			/* Set the fixed height of the footer here */
			height: 60px;
			background-color: #222;
			color: white;
		}
		.container {
			padding-bottom: 10px;
		}
		.margin-bottom-10 {
			margin-bottom: 10px;
		}
		.margin-right-5 {
			margin-right: 5px;
		}
		.form-group {
			width: 100%;
			float: left;
		}
		.col-xs-4 {
			padding-left: 0px;
		}
    </style>
</head>

<body>
	<nav class="navbar navbar-inverse">
	    <ul class="nav navbar-nav">
	        <li><a href="{{ URL::to('stores') }}">Manage Stores</a></li>
	        <li><a href="{{ URL::to('articles') }}">Manage Articles</a></li>
	    </ul>
	</nav>
	<div class="container">
	<h1>Super Shoes management system</h1>
		@yield('content')
    </div>
    <footer class="footer text-center">
      <div class="container">
        <p>ing.cmurillo@gmail.com (c) 2008-2017, (506) 8721-2887</p>
      </div>
    </footer>	
</body>
</html>