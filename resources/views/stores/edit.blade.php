@extends('layouts.app')

@section('content')

<h2>Edit {{ $store->name }}</h2>

{{ Html::ul($errors->all()) }}

{{ Form::model($store, array('route' => array('stores.update', $store->id), 'method' => 'PUT')) }}
    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('name', 'Name *') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('address', 'Address *') }}
            {{ Form::text('address', null, array('class' => 'form-control')) }}
        </div>
    </div>

	<a class="btn btn-small btn-danger" href="{{ URL::to('stores') }}">Cancel</a>
    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection