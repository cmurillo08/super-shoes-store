@extends('layouts.app')

@section('content')

<h2>Create a Store</h2>

{{ Html::ul($errors->all()) }}

{{ Form::open(['url' => 'stores']) }}
    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('name', 'Name *') }}
            {{ Form::text('name', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('address', 'Address *') }}
            {{ Form::text('address', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <a class="btn btn-small btn-danger" href="{{ URL::to('stores') }}">Cancel</a>
    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

{{ Form::close() }}

@endsection