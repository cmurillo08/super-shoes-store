@extends('layouts.app')

@section('content')

<h2>All the stores</h2>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div><a class='btn btn-primary margin-bottom-10' href="{{ URL::to('stores/create') }}">Create New Store</a></div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($stores as $key => $store)
        <tr>
            <td>{{ $store->name }}</td>
            <td>{{ $store->address }}</td>
            <td>
                <a class="pull-left btn btn-info margin-right-5" href="{{ URL::to('stores/' . $store->id . '/edit') }}">Edit</a>
                {{ Form::open(array('url' => 'stores/' . $store->id, 'class' => 'pull-left', 'method' => 'DELETE')) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection