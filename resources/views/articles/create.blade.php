@extends('layouts.app')

@section('content')

<h2>Create an Article</h2>

{{ Html::ul($errors->all()) }}
<!-- requeried fields -->
{{ Form::open(['url' => 'articles']) }}
    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('store', 'Store *') }}
            {{ Form::select('store', $stores, null, ['class' => 'form-control']) }}
        </div>
    </div> 

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('name', 'Name *') }}
            {{ Form::text('name', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('description', 'Description *') }}
            {{ Form::text('description', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('price', 'Price *') }}
            {{ Form::number('price', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('total_in_shelf', 'Total in shelf *') }}
            {{ Form::number('total_in_shelf', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-4">
            {{ Form::label('total_in_vault', 'Total in vault *') }}
            {{ Form::number('total_in_vault', null, ['class' => 'form-control']) }}
        </div>
    </div>            

    <a class="btn btn-small btn-danger" href="{{ URL::to('articles') }}">Cancel</a>
    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

{{ Form::close() }}

@endsection