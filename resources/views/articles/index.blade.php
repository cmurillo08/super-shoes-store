@extends('layouts.app')

@section('content')

<h2>All the articles</h2>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div><a class='btn btn-primary margin-bottom-10' href="{{ URL::to('articles/create') }}">Create New Article</a></div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Total in shelf</th>
            <th>Total in vault</th>
            <th>Store Id</th>
            <th>Action</th> 
        </tr>
    </thead>
    <tbody>
    @foreach($articles as $key => $article)
        <tr>
            <td>{{ $article->name }}</td>
            <td>{{ $article->description }}</td>
            <td>{{ $article->price }}</td>
            <td>{{ $article->total_in_shelf }}</td>
            <td>{{ $article->total_in_vault }}</td>
            <td>{{ $article->store_id }}</td>
            <td>
                <a class="pull-left btn btn-info margin-right-5" href="{{ URL::to('articles/' . $article->id . '/edit') }}">Edit</a>
                {{ Form::open(array('url' => 'articles/' . $article->id, 'class' => 'pull-left', 'method' => 'DELETE')) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection